apply plugin: 'java'
apply plugin: 'maven'
apply plugin: 'pmd'
apply plugin: 'findbugs'
apply plugin: 'checkstyle'
apply plugin: 'cobertura'
apply plugin: 'coveralls'

description = 'Java Chess Protocol Interface'

group = 'com.fluxchess'
version = '1.2.0-alpha'
ext.releaseBranch = 'master'

if (!hasProperty('buildNumber') || !(hasProperty('branchName') && branchName == releaseBranch)) {
  // We're probably building on a dev machine or we're building a branch
  ext.buildNumber = 'dev'
}
if (!hasProperty('revisionNumber')) {
  // We're probably building on a dev machine
  ext.revisionNumber = 'dev'
}

if (!version.contains('-') && !(hasProperty('releaseVersion') && releaseVersion == version)) {
  // Append '-rc' if we're not releasing yet
  version += '-rc'
}

if (version.contains('-')) {
  // Append the buildNumber if we're not releasing
  version += '.' + buildNumber
}

if (version.contains('-') && !(hasProperty('branchName') && branchName == releaseBranch)) {
  // Append the revisionNumber if we're not releasing and not on a release branch
  version += '+' + revisionNumber
}

println "Building version ${version}"

if (!hasProperty('s3AccessKeyId')) {
  ext.s3AccessKeyId = 'n/a'
}

if (!hasProperty('s3SecretAccessKey')) {
  ext.s3SecretAccessKey = 'n/a'
}

buildscript {
  repositories {
    mavenCentral()
  }

  dependencies {
    classpath 'net.saliman:gradle-cobertura-plugin:2.0.0'
    classpath 'org.kt3k.gradle.plugin:coveralls-gradle-plugin:0.1.6'
  }
}

repositories {
  mavenCentral()
}

tasks.withType(Pmd) {
  pmd {
    ignoreFailures = true
  }
}

tasks.withType(FindBugs) {
  findbugs {
    ignoreFailures = true
  }
}

tasks.withType(Checkstyle) {
  checkstyle {
    ignoreFailures = true
    showViolations = false
  }
}

cobertura.coverageFormats = ['html', 'xml']

sourceSets {
  integration
}

dependencies {
  testCompile 'junit:junit:4.+'

  integrationCompile project(':')
  integrationCompile 'junit:junit:4.+'
}

sourceCompatibility = 1.6
targetCompatibility = 1.6

processResources {
  filter(org.apache.tools.ant.filters.ReplaceTokens, tokens: [
    version: project.version,
    buildNumber: project.buildNumber,
    revisionNumber: project.revisionNumber
  ])
}

jar {
  manifest {
    attributes 'Implementation-Title': project.name, 'Implementation-Version': project.version
  }
}

task sourcesJar(type: Jar, dependsOn: classes) {
  classifier = 'sources'
  from sourceSets.main.allSource
}

task javadocJar(type: Jar, dependsOn: javadoc) {
  classifier = 'javadoc'
  from javadoc.destinationDir
}

task integration(type: Test) {
  testClassesDir = sourceSets.integration.output.classesDir
  classpath = sourceSets.integration.runtimeClasspath
}

task dist(type: Zip) {
  def baseDir = "${project.name}-${project.version}"

  into("$baseDir") {
    from 'README.md'
    from 'LICENSE'
    from 'NOTICE'

    from jar
    from sourcesJar
    from javadocJar
  }
}

artifacts {
  archives sourcesJar
  archives javadocJar
}

uploadArchives {
  if (project.hasProperty('branchName')) {
    if (branchName == releaseBranch) {
      if (version.contains('-')) {
        ext.repositoryName = 'staging'
      } else {
        ext.repositoryName = 'release'
      }

      configurations {
        deployerJars
      }

      dependencies {
        deployerJars 'org.springframework.build:aws-maven:4.+'
      }

      repositories {
        mavenDeployer {
          configuration = configurations.deployerJars
          repository(url: "s3://maven.fluxchess.com/${repositoryName}") {
            authentication(userName: s3AccessKeyId, passphrase: s3SecretAccessKey)
          }
        }
      }
    } else {
      doLast { println "Skipping upload. Not on ${releaseBranch} branch (${branchName})." }
    }
  } else {
    doLast { println "Skipping upload. No branch name defined." }
  }
}

task wrapper(type: Wrapper) {
  gradleVersion = '1.8'
}
